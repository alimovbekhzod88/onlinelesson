<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'SiteController@index');
Route::get('/teacher', 'SiteController@teacher');
// Route::get('/admins', function () {
//     return view('admin.dashboard');
// });

// Auth::routes();
Route::get('/meetings', 'BigBlueButtonController@getMeetings')->name('meeting');
Route::get('/meeting/add', 'BigBlueButtonController@addMeeting')->name('meeting.add');
Route::get('/meeting/list', 'BigBlueButtonController@listMeeting')->name('meeting.list');
Route::post('/meeting/create', 'BigBlueButtonController@createMeeting')->name('meeting.create');
Route::get('/meeting/join/{name}/{password}/{meetingID}', 'BigBlueButtonController@joinMeeting')->name('meeting.join');
Route::get('/meeting/info/{password}/{meetingID}', 'BigBlueButtonController@getMeetingInfo')->name('meeting.info');
Route::get('/meeting/close/{password}/{meetingID}', 'BigBlueButtonController@closeMeeting')->name('meeting.close');
Route::get('/meeting/recordings', 'BigBlueButtonController@getRecordings')->name('meeting.recordings');
Route::get('/meeting/recording/delete/{recordId}', 'BigBlueButtonController@deleteRecordings')->name('meeting.recordings-delete');
Route::get('/meeting/running', 'BigBlueButtonController@isMeetingRunning')->name('meeting.running');


// Route::prefix('manage')->middleware('role:administrator|teacher|editor')->group(function () {

//     Route::get('/', 'Admin\ManageController@index');
//     Route::get('/dashboard', 'Admin\ManageController@dashboard')->name('admin.dashboard');

//     // User
//     Route::get('/user', 'UserController@index')->name('user.index');
//     Route::get('/user/create', 'UserController@create')->name('user.create')->middleware('can:create-users');
//     Route::post('/user/store', 'UserController@store')->name('user.store')->middleware('can:create-users');
//     Route::get('/user/edit/{id}', 'UserController@edit')->name('user.edit')->middleware('can:update-users,id');
//     Route::put('/user/update/{id}', 'UserController@update')->name('user.update')->middleware('can:update-users,id');
//     Route::get('/user/show/{id}', 'UserController@show')->name('user.show')->middleware('can:read-users,id');
//     Route::delete('/user/destroy/{id}', 'UserController@destroy')->name('user.destroy')->middleware('can:delete-users,id');

//     Route::resource('/role', 'Admin\RoleController', ['except' => 'destroy'])->middleware('can:create-users');
//     Route::resource('/permission', 'Admin\PermissionController', ['except' => 'destroy'])->middleware('can:create-users');

//     // Meeting
//     Route::get('/meetings', 'BigBlueButtonController@getMeetings')->name('meeting');
//     Route::get('/meeting/add', 'BigBlueButtonController@addMeeting')->name('meeting.add');
//     Route::get('/meeting/list', 'BigBlueButtonController@listMeeting')->name('meeting.list');
//     Route::post('/meeting/create', 'BigBlueButtonController@createMeeting')->name('meeting.create');

//     Route::get('/meeting/join/{name}/{password}/{meetingID}', 'BigBlueButtonController@joinMeeting')->name('meeting.join');
//     Route::get('/meeting/info/{password}/{meetingID}', 'BigBlueButtonController@getMeetingInfo')->name('meeting.info');
//     Route::get('/meeting/close/{password}/{meetingID}', 'BigBlueButtonController@closeMeeting')->name('meeting.close');
//     Route::get('/meeting/recordings', 'BigBlueButtonController@getRecordings')->name('meeting.recordings');
//     Route::get('/meeting/recording/delete/{recordId}', 'BigBlueButtonController@deleteRecordings')->name('meeting.recordings-delete');
//     Route::get('/meeting/running', 'BigBlueButtonController@isMeetingRunning')->name('meeting.running');
// });

// Route::prefix('manage')->middleware('role:user|administrator|teacher')->group(function () {
//     Route::get('/meeting/join/{name}/{password}/{meetingID}', 'BigBlueButtonController@joinMeeting')->name('meeting.join');
//     Route::get('/meeting/info/{password}/{meetingID}', 'BigBlueButtonController@getMeetingInfo')->name('meeting.info');
//     Route::get('/meeting/close/{password}/{meetingID}', 'BigBlueButtonController@closeMeeting')->name('meeting.close');
//     Route::get('/meeting/recordings', 'BigBlueButtonController@getRecordings')->name('meeting.recordings');
//     Route::get('/meeting/recording/delete/{recordId}', 'BigBlueButtonController@deleteRecordings')->name('meeting.recordings-delete');
//     Route::get('/meeting/running', 'BigBlueButtonController@isMeetingRunning')->name('meeting.running');
//     Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
// });

// // Auth::routes();
// Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

// Route::get('/home', 'HomeController@index')->name('home');
