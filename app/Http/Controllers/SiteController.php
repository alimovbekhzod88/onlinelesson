<?php

namespace App\Http\Controllers;

use App\Admin\Role;
use App\Models\Meeting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiteController extends Controller
{
    public function index()
    {
        $meetings = Meeting::all();
        return view("index", compact('meetings'));
    }
    public function teacher()
    {
        $meetings = Meeting::all();
        return view("teacher", compact('meetings'));
    }
}
