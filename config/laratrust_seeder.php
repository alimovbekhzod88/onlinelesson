<?php

return [
    'role_structure' => [
        'administrator' => [
            'users' => 'c,r,u,d',
            'posts' => 'c,r,u,d',
            'meetings' => 'c,r,u,d',
            'products' => 'c,r,u,d',
        ],
        'teacher' => [
            'meetings' => 'c',
        ],
        'editor' => [
            'users' => 'r,u',
            'posts' => 'c,r,u,d',
            'meetings' => 'c,r,u,d',

        ],
        'user' => [],
    ],
    'permission_structure' => [],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
