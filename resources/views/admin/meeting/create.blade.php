@extends('admin.layouts.master')

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

@section('content')
<section id="main-content">
    <section class="wrapper">
        <div class="panel">
            @if (count($errors)>0)
                <div class="alert">
                    <ul class="list-group">
                        @foreach ($errors->all() as $error)
                            <li class="list-group-item text-danger">
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="panel-header">
                <h3 class="text-center" style="padding-top: 20px">
                   Create Meeting
                </h3>
                <hr>
            </div>
            <div class="panel-body">
                <div class="d-flex justify-content-end mb-5">
                    <a href="{{ route('meeting.list') }}" class="btn btn-outline-secondary"><i class="fa fa-arrow-left"></i> Back</a>
                </div>

                <form method="post" action="{{ route('meeting.create') }}">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label for="inputsm">Meeting Name</label>
                    <input class="form-control input-sm" id="meetingName" name="meetingName" type="text" required>
                  </div>
                  <div class="form-group">
                    <label for="inputsm">Meeting Message</label>
                    <input class="form-control input-sm" id="welcomeMessage" name="welcomeMessage" type="text" required>
                  </div>
                   <div class="form-group">
                    <label for="inputdefault">Attendee Password</label>
                    <input class="form-control" id="attendee_password" name="attendee_password" type="text" required>
                  </div>
                  <div class="form-group">
                    <label for="inputlg">Moderator Password</label>
                    <input class="form-control input-sm" id="moderator_password" name="moderator_password" type="text" required>
                  </div>
                  <div class="form-group">
                    <label for="inputlg">Duration</label>
                    <input class="form-control input-sm" id="duration" name="duration" type="number" required>
                  </div>
                  <input type="hidden" class="hidden" name="link">

                  <div class="form-group">
                      <input type="submit" class="btn btn-info" value="Create Meeting">

                  </div>
                </form>
            </div>
        </div>
    </section>
</section>
@endsection
