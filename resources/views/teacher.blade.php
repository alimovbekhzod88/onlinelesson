<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Istok+Web:400,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />

	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="one-page/css/et-line.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link rel="stylesheet" href="css/colors.php?color=0474c4" type="text/css" />

	<!-- Hosting Demo Specific Stylesheet -->
	<link rel="stylesheet" href="demos/course/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="demos/course/course.css" type="text/css" />
	<!-- / -->

	<!-- Document Title
	============================================= -->
	<title>Course Demo | Canvas</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	


			<div class="content-wrap">

				<div class="section topmargin-lg parallax" style=" background-image: url('demos/course/images/icon-pattern.jpg'); background-size: auto; background-repeat: repeat"  data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -500px;">

					<div class="container" style="" >

						<div class="row mt-2">

					
							@foreach ($meetings as $meeting)
								<div class="col-md-3 mb-5" >
									<div class="card course-card hover-effect noborder">
									<a href="{{ route('meeting.join', ['name'=>'Moderator', 'password'=>$meeting->moderator_password, 
										'meetingID'=>$meeting->meetingID]) }}"><img class="card-img-top" src="demos/course/images/courses/1.jpg" alt="Card image cap"></a>
										<div class="card-body">
										<h4 class="card-title t700 mb-2"><a href="{{ route('meeting.join', ['name'=>'Moderator', 'password'=>$meeting->moderator_password, 
										'meetingID'=>$meeting->meetingID]) }}">{{$meeting->meetingName}}</a></h4>
											
										<p class="card-text text-black-50 mb-1">{{$meeting->welcomeMessage}}</p>
										</div>
										<div class="card-footer py-3 d-flex justify-content-between align-items-center bg-white text-muted">
											<div class="badge alert-primary">{{$meeting->duration}}</div>
											<a href="Moderator" class="text-dark position-relative"><i class="icon-line2-user"></i> <span class="author-number">1</span></a>
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>

			</div>

	
	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	

	<!-- External JavaScripts
	============================================= -->
	<script src="js/jquery.js"></script>
	<script src="js/plugins.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="js/functions.js"></script>

</body>
</html>