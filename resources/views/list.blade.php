{{-- @extends('admin.layouts.master') --}}

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

{{-- @section('content') --}}
<section id="main-content">
    <section class="wrapper">
        <div class="panel">
            <div class="panel-header">
                <h3 class="text-center" style="padding-top: 20px">User List</h3>
                <hr>

            </div>
            <div class="panel-body">
                    <div class="d-flex justify-content-end mb-5">
                            <a href="{{ route('meeting.add') }}" class="btn btn-outline-secondary"><i class="fa fa-user-plus"></i> Create User</a>
                    </div>
                    <div class="col-md-12">
                        <table id="table" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>Meeting ID </th>
                                        <th>Teacher Name</th>
                                        <th>Meeting Name</th>
                                        <th>Attendee Password</th>
                                        <th>Moderator Password</th>
                                        <th>Duration</th>
                                        <th>Info</th>
                                        <th>Moderator</th>
                                        {{-- <th>Attendee</th> --}}
                                        <th>Close</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                 
                                    $i=1;
                                    foreach($meetingsList as $k=>$meeting){
                                ?>
                                {{-- {{  dd(auth()->user()->name == 'Administrator') }} --}}
                                        
                                    <tr>
                                        <td>{{ $k++ }}</td>
                                        <td><a href="{{ url('/meeting/info/') }}/<?php echo $meeting->moderator_password;?>/<?php echo $meeting->meetingID;?>"  target="_blank" ><?php echo $meeting -> meetingID;?></a></td>
                                       <td><?php echo $meeting ->user->name;?></td>
                                       <td><?php echo $meeting ->meetingName;?></td>
                                        <td><?php echo $meeting ->moderator_password;?></td>
                                        <td><?php echo $meeting ->attendee_password;?></td>
                                        <td><?php echo $meeting ->duration;?> Min</td>
                                        <td><a href="{{ url('/manage/meeting/info/') }}/<?php echo $meeting->moderator_password;?>/<?php echo $meeting->meetingID;?>"  target="_blank" >info</a></td>
                                        <td><a href="{{ url('/manage/meeting/join/') }}/Moderator <?php echo $i;?>/<?php echo $meeting->moderator_password;?>/<?php echo $meeting->meetingID;?>"  target="_blank" >join</a></td>
                                        <td><a href="{{ url('/meeting/close/') }}/<?php echo $meeting->moderator_password;?>/<?php echo $meeting->meetingID;?>"  target="_blank" >close</a></td>

                                    </tr>
                                  <?php
                                    $i++;
                                    } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>№</th>
                                        <th>Meeting ID </th>
                                        <th>Meeting Name</th>
                                        <th>Attendee Password</th>
                                        <th>Moderator Password</th>
                                        <th>Duration</th>
                                        <th>Info</th>
                                        <th>Moderator</th>
                                        {{-- <th>Attendee</th> --}}
                                        <th>Close</th>
                                    </tr>
                                </tfoot>
                            </table>
                    </div>
            </div>
        </div>
    </section>
</section>
{{-- @endsection --}}
